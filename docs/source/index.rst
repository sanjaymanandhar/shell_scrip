.. documentation documentation master file, created by
   sphinx-quickstart on Thu Nov 18 15:27:45 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to documentation's documentation!
=========================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   Getting started <getting_started>
   More Info <more_info>
   Documentation <documentation>
   Environment setup <environment_setup>

Indices and tables
==================
* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
