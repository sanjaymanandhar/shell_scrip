Start from the beginning
=========================

Getting started
----------------

Setting Up project
++++++++++++++++++++
Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptatem facilis quis eaque laboriosam praesentium consequuntur optio ut. Illo odit necessitatibus ducimus eius magnam omnis illum cum 
dolorum consequatur dolores facere at nihil, perspiciatis saepe aperiam! Saepe pariatur vitae laborum error. Placeat ratione inventore animi a eius quaerat tenetur, laborum pariatur. Ex veniam labore 
dolor nemo, deserunt nisi at doloribus rem? Nulla eveniet repellat corrupti molestias vero nihil perspiciatis accusamus! Aperiam nisi dignissimos eos quia ad! Quidem quos pariatur repudiandae. 


Cloning the project
++++++++++++++++++++
Mollitia excepturi est asperiores molestiae aspernatur, odit eum nobis soluta quisquam expedita tempora porro minima eius fuga vel repellat quaerat temporibus neque recusandae rerum! Voluptatum 
voluptate corporis atque, assumenda cupiditate eos? Velit deleniti, perspiciatis illo laboriosam aut magnam quidem dignissimos, tempora beatae eius quam. Nam distinctio, cumque, est vel molestias quis
ipsam quasi autem corrupti animi ea soluta similique officiis. Repudiandae harum vero quam hic, quaerat tempore pariatur natus enim, dolorem nulla, magnam assumenda veniam aut esse ullam facilis 
aliquam culpa officia libero nisi iure sunt molestiae tenetur laudantium. Quasi, magni quae eos laboriosam amet eveniet. Magni neque et perferendis numquam esse. Officia eaque sed, iure eveniet 
necessitatibus est aperiam? Rerum expedita nemo nihil animi fugit illum, atque ut ducimus. Corrupti!

1. From ssh
2. From Https
3. Mannual installation

.. note::
  installation of the project from a particular method is not mandatory.

.. caution::
  installation of the project from a particular method is not mandatory.

.. danger::
  installation of the project from a particular method is not mandatory.

Running the project
++++++++++++++++++++
.. code-block::

  sudo apt get update
  sudo apt upgrade
  sudp apt another commands

Mollitia excepturi est asperiores molestiae aspernatur, odit eum nobis soluta quisquam expedita tempora porro minima eius fuga vel repellat quaerat temporibus neque recusandae rerum! Voluptatum 
voluptate corporis atque, assumenda cupiditate eos? Velit deleniti, perspiciatis illo laboriosam aut magnam quidem dignissimos, tempora beatae eius quam. Nam distinctio, cumque, est vel molestias quis
ipsam quasi autem corrupti animi ea soluta similique officiis. Repudiandae harum vero quam hic, quaerat tempore pariatur natus enim, dolorem nulla, magnam assumenda veniam aut esse ullam facilis 
aliquam culpa officia libero nisi iure sunt molestiae tenetur laudantium. Quasi, magni quae eos laboriosam amet eveniet. Magni neque et perferendis numquam esse. Officia eaque sed, iure eveniet 
necessitatibus est aperiam? Rerum expedita nemo nihil animi fugit illum, atque ut ducimus. Corrupti!Mollitia excepturi est asperiores molestiae aspernatur, odit eum nobis soluta quisquam expedita tempora porro minima eius fuga vel repellat quaerat temporibus neque recusandae rerum! Voluptatum 
voluptate corporis atque, assumenda cupiditate eos? Velit deleniti, perspiciatis illo laboriosam aut magnam quidem dignissimos, tempora beatae eius quam. Nam distinctio, cumque, est vel molestias quis
ipsam quasi autem corrupti animi ea soluta similique officiis. Repudiandae harum vero quam hic, quaerat tempore pariatur natus enim, dolorem nulla, magnam assumenda veniam aut esse ullam facilis 
aliquam culpa officia libero nisi iure sunt molestiae tenetur laudantium. Quasi, magni quae eos laboriosam amet eveniet. Magni neque et perferendis numquam esse. Officia eaque sed, iure eveniet 
necessitatibus est aperiam? Rerum expedita nemo nihil animi fugit illum, atque ut ducimus. Corrupti!

``SUDO APT COMMAND``

